(** * The Category of Functors *)

(** Objects are functors (between some fixed categories [C] and [D]);
    morphisms are natural transformations. *)

From Kittens Require Import
  Core.Settings
  Core.Core
  Core.Tensor.

Lemma id_comm {C : Category} a b (f : hom C a b) : f >>> id C =~ id C >>> f.
Proof.
  rewrite cat_id_l; apply cat_id_r.
Qed.

Section FUNCTOR.

Context (C D : Category).

Definition eq_Natural {F G : Functor C D} (n m : Natural F G) : Prop :=
  forall a, trans n (a := a) =~ trans m.

Definition id_Natural (F : Functor C D) : Natural F F :=
  {| trans := fun _ => id D
  ;  trans_natural := fun _ _ _ => id_comm _ _ _
  |}.

Definition cat_Natural (F G H : Functor C D) (n : Natural F G) (m : Natural G H) : Natural F H.
Proof. refine
  {| trans := fun _ => cat D (trans n) (trans m) |}.
  intros. rewrite <- cat_assoc, trans_natural, cat_assoc, trans_natural, <- cat_assoc.
  reflexivity.
Defined.

Lemma Equivalence_eq_Natural (F G : Functor C D) : Equivalence (@eq_Natural F G).
Proof.
  constructor; repeat intro.
  - reflexivity.
  - symmetry; auto.
  - etransitivity; eauto.
Qed.

Lemma Proper_cat_Natural {F G H : Functor C D}
  : Proper (eq_Natural ==> eq_Natural ==> eq_Natural) (@cat_Natural F G H).
Proof.
  intros n1 n2 En m1 m2 Em a; cbn.
  apply (Proper_cat D); auto.
Qed.

Lemma cat_id_l_Natural {F G : Functor C D} (n : Natural F G)
  : eq_Natural (cat_Natural id_Natural n) n.
Proof.
  intros a; cbn; apply cat_id_l.
Qed.

Lemma cat_id_r_Natural {F G : Functor C D} (n : Natural F G)
  : eq_Natural (cat_Natural n id_Natural) n.
Proof.
  intros a; cbn; apply cat_id_r.
Qed.

Lemma cat_assoc_Natural {F1 F2 F3 F4 : Functor C D}
    (n1 : Natural F1 F2) (n2 : Natural F2 F3) (n3 : Natural F3 F4)
  : eq_Natural (cat_Natural (cat_Natural n1 n2) n3) (cat_Natural n1 (cat_Natural n2 n3)).
Proof.
  intros a; cbn; apply cat_assoc.
Qed.

Definition FUNCTOR : Category :=
  {| obj := Functor C D
  ;  hom := Natural
  ;  eq_hom := @eq_Natural
  ;  Equivalence_eq_hom := @Equivalence_eq_Natural
  ;  id := @id_Natural
  ;  cat := @cat_Natural
  ;  Proper_cat := @Proper_cat_Natural
  ;  cat_id_l := @cat_id_l_Natural
  ;  cat_id_r := @cat_id_r_Natural
  ;  cat_assoc := @cat_assoc_Natural
  |}.

End FUNCTOR.

Section Compose_laws.

Local Notation X :=
  {| trans := fun _ => id _
  ;  trans_natural := fun _ _ _ => id_comm _ _ _
  |}.

Section Compose_Id.

Context (C D : Category) (F : Functor C D).

Definition Compose_Id_l : Natural (Compose Id F) F := X.
Definition Compose_Id_r : Natural (Compose F Id) F := X.
Definition co_Compose_Id_l : Natural F (Compose Id F) := X.
Definition co_Compose_Id_r : Natural F (Compose F Id) := X.

End Compose_Id.

Section Compose_assoc.

Context (C1 C2 C3 C4 : Category) (F3 : Functor C3 C4) (F2 : Functor C2 C3) (F1 : Functor C1 C2).

Definition Compose_assoc : Natural (Compose (Compose F3 F2) F1) (Compose F3 (Compose F2 F1)) := X.
Definition co_Compose_assoc : Natural (Compose F3 (Compose F2 F1)) (Compose (Compose F3 F2) F1) := X.

End Compose_assoc.
End Compose_laws.

(** The [COMPOSE] tensor product in the category of endofunctors. *)
Section ENDOFUNCTOR.

Context (C : Category) (FC := FUNCTOR C C).

Definition COMPOSE_F : Category.Product.Product FC FC -> FC :=
  fun (fg : Category.Product.Product FC FC) => Compose (fst fg) (snd fg).

Definition COMPOSE_map_trans {F1 F2 G1 G2 : Functor C C}
    (f : forall a, hom C (F1 a) (F2 a)) (g : forall a, hom C (G1 a) (G2 a))
  : forall a, hom C (Compose F1 G1 a) (Compose F2 G2 a) :=
  fun a => f (G1 a) >>> map F2 (g a).

Global Arguments COMPOSE_map_trans {_ _ _ _} f g _ /.

Theorem COMPOSE_map_trans_natural {F1 F2 G1 G2 : Functor C C}
    (f : Natural F1 F2) (g : Natural G1 G2)
  : forall a b h,
       map (Compose F1 G1) h >>> COMPOSE_map_trans f g b
    =~ COMPOSE_map_trans f g a >>> map (Compose F2 G2) h.
Proof.
  intros. unfold COMPOSE_map_trans; cbn.
  rewrite <- cat_assoc, trans_natural.
  rewrite cat_assoc, <- map_cat, trans_natural, map_cat, <- cat_assoc.
  reflexivity.
Qed.

Definition COMPOSE_map (a b : Category.Product.Product FC FC) (fg : hom _ a b)
  : hom FC (COMPOSE_F a) (COMPOSE_F b) :=
  {| trans := COMPOSE_map_trans (fst fg) (snd fg)
  ;  trans_natural := COMPOSE_map_trans_natural (fst fg) (snd fg)
  |}.

Lemma COMPOSE_Proper_map a b
  : Proper (eq_hom _ ==> eq_hom FC) (@COMPOSE_map a b).
Proof.
  intros f1 f2 [Ef Eg] x; cbn.
  apply Proper_cat; [ apply (Ef _) | apply Proper_map, (Eg _) ].
Qed.

Lemma COMPOSE_map_id a : @COMPOSE_map a a (id _) =~ id FC.
Proof.
  intros x; cbn. rewrite cat_id_l; apply map_id.
Qed.

Lemma COMPOSE_map_cat a b c (f : hom _ a b) (g : hom _ b c)
  : COMPOSE_map (f >>> g) =~ COMPOSE_map f >>> COMPOSE_map g.
Proof.
  intros x; cbn in *. rewrite map_cat.
  rewrite <- 2 cat_assoc. apply Proper_cat; [ | reflexivity ].
  rewrite cat_assoc, <- trans_natural, <- cat_assoc.
  reflexivity.
Qed.

Definition COMPOSE_Functor : Functor (Category.Product.Product FC FC) FC :=
  {| Functor.F := COMPOSE_F
  ;  map := @COMPOSE_map
  ;  Proper_map := @COMPOSE_Proper_map
  ;  map_id := COMPOSE_map_id
  ;  map_cat := @COMPOSE_map_cat
  |}.

Definition COMPOSE : Tensor (FUNCTOR C C) :=
  {| Tensor.F := COMPOSE_Functor
  ;  I := @Id C
  ;  unit_l := @Compose_Id_l C C
  ;  unit_r := @Compose_Id_r C C
  ;  counit_l := @co_Compose_Id_l C C
  ;  counit_r := @co_Compose_Id_r C C
  ;  assoc := @Compose_assoc C C C C
  ;  coassoc := @co_Compose_assoc C C C C
  |}.

End ENDOFUNCTOR.
