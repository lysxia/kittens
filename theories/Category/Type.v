(** * The Category of Types *)

(** When a category theory textbook says "the category of sets",
    types can almost always be used interchangeably. *)

(** Objects are types; morphisms are functions. *)

From Coq Require Import
  List.
Import ListNotations.

From Kittens Require Import
  Core.Settings
  Core.Core
  Core.Monad.

Definition eq_fun (a b : Type) (f g : a -> b) : Prop :=
  forall x, f x = g x.

Definition Equivalence_fun {a b : Type} : Equivalence (@eq_fun a b) :=
  Equivalence.pointwise_equivalence eq_equivalence.

Import EqNotations.

Definition ap_eq {a b : Type} {f g : a -> b} {x y : a} (Ef : eq_fun f g) (Ex : x = y) : f x = g y :=
  rew [fun y => f x = g y] Ex in
  Ef x.

Definition TYPE : Category :=
  {| obj := Type
   ; hom := fun a b => a -> b
   ; eq_hom := @eq_fun
   ; Equivalence_eq_hom := @Equivalence_fun
   ; id := fun a x => x
   ; cat := fun a b c f g x => g (f x)
   ; Proper_cat := fun a b c f f' Ef g g' Eg x =>
       ap_eq Eg (ap_eq Ef eq_refl)
   ; cat_id_l := fun a b f x => eq_refl
   ; cat_id_r := fun a b f x => eq_refl
   ; cat_assoc := fun a b c d f g h x => eq_refl
  |}.

Canonical TYPE.
