(** * The Category of Monoids *)

(** Objects are monoids; morphisms are monoid morphisms. *)

From Coq Require Import
  List.
Import ListNotations.

From Kittens Require Import
  Core.Settings
  Core.Core
  Category.Type.

Record Monoid : Type :=
  { carrier :> Type
  ; append : carrier -> carrier -> carrier
  ; empty : carrier
  ; append_empty_l : forall x, append empty x = x
  ; append_empty_r : forall x, append x empty = x
  ; append_assoc : forall x y z, append (append x y) z = append x (append y z)
  }.

Record monoid_morphism (a b : Monoid) :=
  { morphism_carrier :> a -> b
  ; morphism_empty : morphism_carrier (empty a) = empty b
  ; morphism_append : forall {x y : a},
      morphism_carrier (append a x y) = append b (morphism_carrier x) (morphism_carrier y)
  }.

Definition Equivalence_projection {a b : Type} (f : a -> b) (r : b -> b -> Prop)
    (EQ : Equivalence r)
  : Equivalence (fun x y => r (f x) (f y)).
Proof.
  constructor.
  - intros x; reflexivity.
  - intros x y H; symmetry; apply H.
  - intros x y z H1 H2; etransitivity; eassumption.
Qed.

Definition id_morphism (a : Monoid) : monoid_morphism a a :=
  {| morphism_carrier := fun x => x
   ; morphism_empty := eq_refl
   ; morphism_append := fun x y => eq_refl
  |}.

Definition cat_morphism (a b c : Monoid) (f : monoid_morphism a b) (g : monoid_morphism b c)
  : monoid_morphism a c :=
  {| morphism_carrier := fun x => g (f x)
   ; morphism_empty := (f_equal g (morphism_empty f) >>> morphism_empty g)%eq
   ; morphism_append := fun x y => (f_equal g (morphism_append f) >>> morphism_append g)%eq
  |}.

Definition MONOID : Category :=
  {| obj := Monoid
   ; hom := monoid_morphism
   ; eq_hom := @eq_fun
   ; Equivalence_eq_hom := fun a b => Equivalence_projection morphism_carrier Equivalence_fun
   ; id := @id_morphism
   ; cat := @cat_morphism
   ; Proper_cat := fun a b c f f' Ef g g' Eg x =>
       ap_eq Eg (ap_eq Ef eq_refl)
   ; cat_id_l := fun a b f x => eq_refl
   ; cat_id_r := fun a b f x => eq_refl
   ; cat_assoc := fun a b c d f g h x => eq_refl
  |}.

Canonical MONOID.
