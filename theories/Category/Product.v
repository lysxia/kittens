(** * The Product of Two Categories *)

(** Objects are pairs of objects [(c, d)];
    morphisms are pairs of morphisms [(f, g)]. *)

From Kittens Require Import
  Core.Settings
  Core.Core.

Section Product.

Context (C D : Category).

Definition hom_Product : C * D -> C * D -> Type :=
  fun o1 o2 => (hom C (fst o1) (fst o2) * hom D (snd o1) (snd o2))%type.

Definition eq_Product a b (f g : hom_Product a b) : Prop :=
  (fst f =~ fst g) /\ (snd f =~ snd g).

Lemma Equivalence_eq_Product {a b} : Equivalence (@eq_Product a b). 
Proof.
Admitted.

Definition id_Product {a} : hom_Product a a :=
  (id C, id D).

Definition cat_Product a b c (f : hom_Product a b) (g : hom_Product b c) : hom_Product a c :=
  (cat C (fst f) (fst g), cat D (snd f) (snd g)).

Lemma Proper_cat_Product a b c
  : Proper (eq_Product ==> eq_Product ==> eq_Product) (@cat_Product a b c).
Proof.
Admitted.

Lemma cat_id_l_Product a b (f : hom_Product a b)
  : eq_Product (cat_Product id_Product f) f.
Proof.
Admitted.

Lemma cat_id_r_Product a b (f : hom_Product a b)
  : eq_Product (cat_Product f id_Product) f.
Proof.
Admitted.

Lemma cat_assoc_Product a b c d (f : hom_Product a b) (g : hom_Product b c) (h : hom_Product c d)
  : eq_Product (cat_Product (cat_Product f g) h) (cat_Product f (cat_Product g h)).
Proof.
Admitted.

Definition Product : Category :=
  {| obj := (C * D)%type
  ;  hom := hom_Product
  ;  eq_hom := @eq_Product
  ;  Equivalence_eq_hom := @Equivalence_eq_Product
  ;  id := @id_Product
  ;  cat := @cat_Product
  ;  Proper_cat := @Proper_cat_Product
  ;  cat_id_l := @cat_id_l_Product
  ;  cat_id_r := @cat_id_r_Product
  ;  cat_assoc := @cat_assoc_Product
  |}.

End Product.

Definition bimap {C D E : Category} (F : Functor (Product C D) E) {a1 a2 : C} {b1 b2 : D}
    (f : hom C a1 a2) (g : hom D b1 b2) : hom E (F (a1, b1)) (F (a2, b2)) :=
  map (C := Product _ _) F (a := (a1, b1)) (b := (a2, b2))
    (pair (A := hom _ a1 a2) (B := hom _ b1 b2) f g).
