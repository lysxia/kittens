(** * Monad in the Category of Types *)

(** What Haskellers call "Monad" *)

From Kittens Require Import
  Core.Settings
  Core.Core
  Core.Monad
  Category.Type.

Class TypeMonad (m : Type -> Type) : Type :=
  { tret : forall {a}, a -> m a
  ; tjoin : forall {a}, m (m a) -> m a
  ; tmap : forall {a b}, (a -> b) -> m a -> m b
  ; tret_tjoin : forall {a} (u : m a),
      tjoin (tret u) = u
  ; tmap_tret_tjoin : forall {a} (u : m a),
      tjoin (tmap tret u) = u
  ; tjoin_tjoin : forall {a} (u : m (m (m a))),
      tjoin (tmap tjoin u) = tjoin (tjoin u)
  ; tret_tmap : forall {a b} (f : a -> b) (x : a),
      tmap f (tret x) = tret (f x)
  ; tjoin_tmap : forall {a b} (f : a -> b) (u : m (m a)),
      tmap f (tjoin u) = tjoin (tmap (tmap f) u)
  ; tmap_id : forall {a} (u : m a),
      tmap (fun x => x) u = u
  ; tmap_compose : forall {a b c} (f : a -> b) (g : b -> c) (u : m a),
      tmap (fun x => g (f x)) u = tmap g (tmap f u)
  }.

Definition bind {m : Type -> Type} `{TypeMonad m} {a b : Type}
    (u : m a) (k : a -> m b) : m b :=
  tjoin (tmap k u).

Declare Scope monad_scope.
Delimit Scope monad_scope with monad.
Local Open Scope monad_scope.

Infix ">>=" := bind (at level 60) : monad_scope.

Definition TypeMonad_Monad (M : Monad TYPE) : TypeMonad M :=
  {| tret := fun a => ret M
   ; tjoin := fun a => join M
   ; tmap := fun a b f => map M f
   ; tret_tjoin := fun a => ret_join M
   ; tmap_tret_tjoin := fun a => map_ret_join M
   ; tjoin_tjoin := fun a => join_join M
   ; tret_tmap := fun a b f => symmetry (natural_ret M f)
   ; tjoin_tmap := fun a b f => symmetry (natural_join M f)
   ; tmap_id := fun a => map_id M
   ; tmap_compose := fun a b c => map_cat M
  |}.
