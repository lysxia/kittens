(** * Free Monoid *)

(** Lists are the free monoid: there is an adjunction [LIST |- FORGET],
    and we get a monad for free. *)

From Coq Require Import
  List.
Import ListNotations.

From Kittens Require Import
  Core.Settings
  Core.Core
  Core.Adjunction
  Core.Monad
  Core.MonadFacts
  Category.Type
  Category.Monoid
  Kitty.TypeMonad.

Definition list_monoid (a : Type) : Monoid :=
  {| carrier := list a
   ; empty := nil
   ; append := app (A := a)
   ; append_empty_l := app_nil_l (A := a)
   ; append_empty_r := app_nil_r (A := a)
   ; append_assoc := fun x y z => eq_sym (app_assoc (A := a) x y z)
  |}.

Canonical list_monoid.

Definition map_list_monoid (a b : Type) (f : a -> b)
  : monoid_morphism (list_monoid a) (list_monoid b) :=
  {| morphism_carrier := List.map f
   ; morphism_empty := eq_refl
   ; morphism_append := List.map_app f
  |}.

Definition LIST : Functor TYPE MONOID :=
  {| Functor.F := list_monoid
   ; map := fun a b => map_list_monoid
   ; Proper_map := fun a b f f' Ef => List.map_ext f f' Ef
   ; map_id := List.map_id
   ; map_cat := fun a b c f g l => eq_sym (map_map f g l)
  |}.

Definition FORGET : Functor MONOID TYPE :=
  {| Functor.F := carrier
   ; map := fun a b => morphism_carrier
   ; Proper_map := fun a b f f' Ef => Ef
   ; map_id := fun a x => eq_refl
   ; map_cat := fun a b c f g l => eq_refl
  |}.

Fixpoint fold (M : Monoid) (xs : list M) : M :=
  match xs with
  | [] => empty M
  | x :: xs => append M x (fold M xs)
  end.

Lemma fold_append (M : Monoid) (xs ys : list M)
  : fold M (xs ++ ys) = append M (fold M xs) (fold M ys).
Proof.
  induction xs; cbn.
  - rewrite append_empty_l. reflexivity.
  - rewrite append_assoc. f_equal. assumption.
Qed.

Definition fold_monoid_morphism (M : Monoid) : monoid_morphism (LIST (FORGET M)) M :=
  {| morphism_carrier := fold M
   ; morphism_empty := eq_refl
   ; morphism_append := fold_append M
  |}.

Definition Adjunction_LIST_FORGET : Adjunction LIST FORGET.
Proof. refine
  {| counit := @fold_monoid_morphism
   ; unit := fun a x => [x]
  |}.
  - (* FGF *) intros a. cbn. intros xs.
    induction xs; cbn.
    + reflexivity.
    + f_equal. assumption.
  - (* GFG *) intros M. cbn. intros xs.
    apply append_empty_r.
  - (* natural_counit *) intros M N f. cbn. intros xs.
    induction xs; cbn.
    + rewrite morphism_empty. reflexivity.
    + rewrite morphism_append. f_equal. assumption.
  - (* natural_unit *) intros a b f. cbn. intros x.
    reflexivity.
Defined.

Instance TypeMonad_list : TypeMonad list :=
  TypeMonad_Monad (Monad_Adjunction Adjunction_LIST_FORGET).

Print Assumptions TypeMonad_list.

Compute ([1; 2; 3] >>= fun n => repeat n n)%monad.
Compute (tret 3 : list nat).
