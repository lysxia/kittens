(** * Internal settings for the whole library *)

Export Set Primitive Projections.

Export Set Implicit Arguments.
Export Set Contextual Implicit.
Export Set Maximal Implicit Insertion.

Export Set Warnings "-projection-no-head-constant,-notation-overridden".

From Coq Require Export
  Setoid
  Morphisms.

Declare Scope cat_scope.
Delimit Scope cat_scope with cat.

Global Open Scope cat_scope.
(* Only opened when this module is imported, i.e., either explicitly or
   re-[Export]-ed by an imported module. *)

Record prod (A B : Type) : Type := pair { fst : A ; snd : B }.
Infix "*" := prod : type_scope.
Notation "( x , y , .. , z )" := (pair .. (pair x y) .. z) : core_scope.
