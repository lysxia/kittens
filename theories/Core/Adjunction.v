(** * Adjunctions *)

From Kittens Require Import
  Core.Settings
  Core.Core.

(* Unit-counit adjunction *)
Record Adjunction C D (F : Functor D C) (G : Functor C D) : Type :=
  { counit : forall {a : C},
      hom C (F (G a)) a
  ; unit : forall {a : D},
      hom D a (G (F a))
  ; adj_FGF : forall {a : D},
      map F unit >>> counit =~ id C (a := F a)
  ; adj_GFG : forall {a : C},
      unit >>> map G counit =~ id D (a := G a)
  ; natural_counit : forall {a b : C} (f : hom C a b),
      map F (map G f) >>> counit =~ counit >>> f
  ; natural_unit : forall {a b : D} (f : hom D a b),
      f >>> unit =~ unit >>> map G (map F f)
  }.

Arguments counit {C D F G} A {a} : rename.
Arguments unit {C D F G} A {a} : rename.

(* Extra (unused): Hom-set adjunction *)
Module Export HAdjunction.
Record HAdjunction C D (F : Functor D C) (G : Functor C D) : Type :=
  { forget : forall {a : D} {b : C},
      hom C (F a) b -> hom D a (G b)
  ; generate : forall {a : D} {b : C},
      hom D a (G b) -> hom C (F a) b
  ; forget_generate : forall {a : D} {b : C} (h : hom C (F a) b),
      generate (forget h) =~ h
  ; generate_forget : forall {a : D} {b : C} (h : hom D a (G b)),
      forget (generate h) =~ h
    (* TODO: naturality *)
  }.
End HAdjunction.
