From Kittens Require Import
  Core.Settings
  Core.Core
  Category.Functor.

Record Inverses {C : Category} {a b : C} (f : hom C a b) (g : hom C b a) : Prop :=
  { inverse_fg : f >>> g =~ id C
  ; inverse_gf : g >>> f =~ id C
  }.

Definition flip_Inverses {C : Category} {a b : C} {f : hom C a b} {g : hom C b a}
    (ii : Inverses f g) : Inverses g f :=
  {| inverse_fg := inverse_gf ii
  ;  inverse_gf := inverse_fg ii
  |}.

Theorem flip_Inverses_involutive {C : Category} {a b : C} (f : hom C a b) (g : hom C b a)
    (ii : Inverses f g)
  : flip_Inverses (flip_Inverses ii) = ii.
Proof.
  reflexivity.
Qed.

Definition id_Inverses (C : Category) (a : C) : Inverses (id C (a := a)) (id C) :=
  {| inverse_fg := cat_id_l C _
  ;  inverse_gf := cat_id_r C _
  |}.

Lemma cat_Inverses_fg (C : Category) (a b c : C) (f : hom C a b) f' (g : hom C b c) g'
    (i_f : Inverses f f') (i_g : Inverses g g')
  : (f >>> g) >>> (g' >>> f') =~ id C.
Proof.
  rewrite cat_assoc, <- (cat_assoc C g), (inverse_fg i_g).
  rewrite cat_id_l; apply (inverse_fg i_f).
Qed.

Definition cat_Inverses {C : Category} {a b c : C} (f : hom C a b) f' (g : hom C b c) g'
    (i_f : Inverses f f') (i_g : Inverses g g')
  : Inverses (f >>> g) (g' >>> f') :=
  {| inverse_fg := cat_Inverses_fg i_f i_g
  ;  inverse_gf := cat_Inverses_fg (flip_Inverses i_g) (flip_Inverses i_f)
  |}.

Record Iso (C : Category) (a b : C) : Type :=
  { iso :> hom C a b
  ; coiso : hom C b a
  ; Inverses_iso : Inverses iso coiso
  }.

Record NaturalIso {C D : Category} (F G : Functor C D) : Type :=
  { iso_trans :> Natural F G
  ; coiso_trans : Natural G F
  ; Inverses_NaturalIso : forall (a : C), Inverses (iso_trans a) (coiso_trans a)
  }.

Definition id_NaturalIso {C D : Category} {F : Functor C D} : NaturalIso F F :=
  {| iso_trans := id_Natural
  ;  coiso_trans := id_Natural
  ;  Inverses_NaturalIso := fun _ => id_Inverses
  |}.

Definition cat_NaturalIso {C D : Category} {F G H : Functor C D}
    (niF : NaturalIso F G) (niG : NaturalIso G H) : NaturalIso F H :=
  {| iso_trans := cat_Natural niF niG
  ;  coiso_trans := cat_Natural (coiso_trans niG) (coiso_trans niF)
  ;  Inverses_NaturalIso := fun _ => cat_Inverses (Inverses_NaturalIso _ _) (Inverses_NaturalIso _ _)
  |}.
