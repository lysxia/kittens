(** * Category Theory Basics *)

(**
- [Category]
- [Functor]
- [Natural] transformations
- Functor [Id], [Compose]
 *)

From Kittens Require Import
  Core.Settings.

Module Export Category.
Record Category : Type :=
  { obj :> Type
  ; hom : obj -> obj -> Type
  ; eq_hom : forall {a b : obj},
      hom a b -> hom a b -> Prop
  ; Equivalence_eq_hom : forall {a b : obj},
      Equivalence (@eq_hom a b)
  ; id : forall {a : obj},
      hom a a
  ; cat : forall {a b c : obj},
      hom a b -> hom b c -> hom a c
  ; Proper_cat : forall {a b c : obj},
      Proper (eq_hom ==> eq_hom ==> eq_hom) (@cat a b c)
  ; cat_id_l : forall {a b : obj} (f : hom a b),
      eq_hom (cat id f) f
  ; cat_id_r : forall {a b : obj} (f : hom a b),
      eq_hom (cat f id) f
  ; cat_assoc : forall {a b c d : obj} (f : hom a b) (g : hom b c) (h : hom c d),
      eq_hom (cat (cat f g) h) (cat f (cat g h))
  }.
End Category.

Existing Instance Equivalence_eq_hom.
Existing Instance Proper_cat.

Generalizable Variables C D E.
Implicit Types C D E : Category.

Infix "=~" := (eq_hom _) (at level 70) : cat_scope.
Infix ">>>" := (cat _) (at level 50) : cat_scope.

Module Export Functor.
Record Functor (C D : Category) : Type :=
  { F :> C -> D
  ; map : forall {a b : C},
      hom C a b -> hom D (F a) (F b)
  ; Proper_map : forall {a b : C},
      Proper (eq_hom C ==> eq_hom D) (@map a b)
  ; map_id : forall {a : C},
      map (id C (a := a)) =~ id D
  ; map_cat : forall {a b c : C} (f : hom C a b) (g : hom C b c),
      map (f >>> g) =~ map f >>> map g
  }.
End Functor.

Existing Instance Proper_map.

Generalizable Variables F G M.

Module Export Natural.
Record Natural C D (F G : Functor C D) : Type :=
  { trans :> forall {a}, hom D (F a) (G a)
  ; trans_natural : forall a b (f : hom C a b), map F f >>> trans =~ trans >>> map G f
  }.
Arguments Build_Natural C D F G &.
End Natural.

Declare Scope eq_scope.
Delimit Scope eq_scope with eq.
Infix ">>>" := transitivity : eq_scope.

Definition Compose `(G : Functor D E) `(F : Functor C D) : Functor C E :=
  {| Functor.F := fun c => G (F c)
   ; map := fun a b f => map G (map F f)
   ; Proper_map := fun a b f f' Ef => Proper_map G _ _ (Proper_map F f f' Ef)
   ; map_id := fun a => (Proper_map G _ _ (map_id F) >>> map_id G)%eq
   ; map_cat := fun a b c f g => (Proper_map G _ _ (map_cat F f g) >>> map_cat G _ _)%eq
  |}.

Definition Id (C : Category) : Functor C C :=
  {| Functor.F := fun a => a
  ;  map := fun _ _ h => h
  ;  Proper_map := fun _ _ _ _ Eh => Eh
  ;  map_id := fun _ => reflexivity _
  ;  map_cat := fun _ _ _ _ _ => reflexivity _
  |}.

(* [rw] vs [rewrite]:
   Con:
     [rw] requires an equation with the same LHS as the goal,
     whereas [rewrite] only needs an equation on a subterm of the goal (whether LHS or
     RHS, it doesn't even have to be an equation) and automatically infers a
     [Proper] instance for the context;
   Pro:
     [rw] can unify the LHSs of the given equation and the goal up to convertibility,
     whereas [rewrite] pretty much requires the LHSs to match exactly. *)
Tactic Notation "rw" uconstr(H) := apply (transitivity H).
Tactic Notation "rw" "<-" uconstr(H) := apply (transitivity (symmetry H)).
