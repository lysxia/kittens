(** * Facts about Monads *)

(**
- Adjunctions induce monads
  ([Monad_Adjunction]).

- Monads are monoids in the category of endofunctors
  ([Monoid_of_Monad], [Monad_of_Monoid]).
*)

From Kittens Require Import
  Core.Settings
  Core.Core
  Core.Monad
  Core.Adjunction
  Core.Tensor
  Core.MonoidObject
  Category.Functor.

Definition Monad_Adjunction C D (F : Functor D C) (G : Functor C D) (FG : Adjunction F G)
  : Monad D :=
  {| asFunctor := Compose G F
   ; ret := fun a => unit FG (a := a)
   ; join := fun a => map G (counit FG (a := F a))
   ; ret_join := fun a => adj_GFG FG
   ; map_ret_join := fun a =>
       ( symmetry (map_cat G _ _) >>>
         Proper_map G _ _ (adj_FGF FG) >>>
         map_id G )%eq
   ; join_join := fun a =>
       ( symmetry (map_cat G _ _) >>>
         Proper_map G _ _ (natural_counit FG _) >>>
         map_cat G _ _ )%eq
   ; natural_ret := fun a b f => natural_unit FG f
   ; natural_join := fun a b f =>
       ( symmetry (map_cat G _ _) >>>
         Proper_map G _ _ (natural_counit FG _) >>>
         map_cat G _ _ )%eq
  |}.

Section Monoid_of_Monad.

Context (C : Category).
Context (M : Monad C).
Context (T := COMPOSE (C := C)).

Definition Monoid_of_Monad_mul : Natural (Compose M M) M :=
  {| trans := @join _ M
  ;  trans_natural := fun _ _ _ => natural_join M _
  |}.

Definition Monoid_of_Monad_one : Natural Id M :=
  {| trans := @ret _ M
  ;  trans_natural := fun _ _ _ => natural_ret M _
  |}.

Notation mul := Monoid_of_Monad_mul.
Notation one := Monoid_of_Monad_one.

Lemma Monoid_of_Monad_mul_one_l
  : counit_l T >>> Category.Product.bimap T one (id _) >>> mul =~ id _.
Proof.
  intros a; cbn. rewrite cat_id_l, map_id, cat_id_r.
  apply ret_join.
Qed.

Lemma Monoid_of_Monad_mul_one_r
  : counit_r T >>> Category.Product.bimap T (id _) one >>> mul =~ id _.
Proof.
  intros a; cbn. rewrite 2 cat_id_l.
  apply map_ret_join.
Qed.

Lemma Monoid_of_Monad_mul_assoc
  : Category.Product.bimap T mul (id _) >>> mul
  =~ (assoc T >>> Category.Product.bimap T (id _) mul) >>> mul.
Proof.
  intros a; cbn. rewrite 2 cat_id_l, map_id, cat_id_r.
  symmetry; apply join_join.
Qed.

Definition Monoid_of_Monad : MonoidObject (C := FUNCTOR C C) COMPOSE :=
  {| MonoidObject.M := M
  ;  MonoidObject.mul := Monoid_of_Monad_mul
  ;  MonoidObject.one := Monoid_of_Monad_one
  ;  mul_one_l := Monoid_of_Monad_mul_one_l
  ;  mul_one_r := Monoid_of_Monad_mul_one_r
  ;  mul_assoc := Monoid_of_Monad_mul_assoc
  |}.

End Monoid_of_Monad.

Section Monad_of_Monoid.

Context (C : Category).
Context (T := COMPOSE (C := C)).
Context (MO : MonoidObject (C := FUNCTOR C C) T).
Context (M := MonoidObject.M MO).

Definition Monad_of_Monoid_join : forall a, hom C (M (M a)) (M a) :=
  fun _ => trans (mul MO).

Definition Monad_of_Monoid_ret : forall a, hom C a (M a) :=
  fun _ => trans (one MO).

Notation join := Monad_of_Monoid_join.
Notation ret := Monad_of_Monoid_ret.

Lemma Monad_of_Monoid_ret_join a : ret (M a) >>> join a =~ id C.
Proof.
  assert (H := mul_one_l MO a); cbn in H.
  rewrite cat_id_l, map_id, cat_id_r in H. exact H.
Qed.

Lemma Monad_of_Monoid_map_ret_join a : map M (ret a) >>> join a =~ id C.
Proof.
  assert (H := mul_one_r MO a); cbn in H.
  rewrite 2 cat_id_l in H. exact H.
Qed.

Lemma Monad_of_Monoid_join_join a
  : map M (join a) >>> join a =~ join (M a) >>> join a.
Proof.
  assert (H := mul_assoc MO a); cbn in H.
  rewrite 2 cat_id_l, map_id, cat_id_r in H. symmetry; exact H.
Qed.

Definition Monad_of_Monoid : Monad C :=
  {| asFunctor := M
  ;  Monad.ret := ret
  ;  Monad.join := join
  ;  ret_join := Monad_of_Monoid_ret_join
  ;  map_ret_join := Monad_of_Monoid_map_ret_join
  ;  join_join := Monad_of_Monoid_join_join
  ;  natural_ret := trans_natural (one MO)
  ;  natural_join := trans_natural (mul MO)
  |}.

End Monad_of_Monoid.
