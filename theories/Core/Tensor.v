(** * Monoidal Categories *)

(** Monoidal categories are categories equipped with a binary operation on
    objects, called tensor product. *)

From Kittens Require Import
  Core.Settings
  Core.Core
  Category.Product.

Record Tensor (C : Category) : Type :=
  { F :> Functor (Product C C) C
  ; I : C
  ;   unit_l : forall {a : C}, hom C (F (I, a)) a
  ; counit_l : forall {a : C}, hom C a (F (I, a))
  ;   unit_r : forall {a : C}, hom C (F (a, I)) a
  ; counit_r : forall {a : C}, hom C a (F (a, I))
  ;   assoc : forall {a b c : C}, hom C (F (F (a, b), c)) (F (a, F (b, c)))
  ; coassoc : forall {a b c : C}, hom C (F (a, F (b, c))) (F (F (a, b), c))
  }.
