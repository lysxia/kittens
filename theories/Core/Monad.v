(** * Monads *)

From Kittens Require Import
  Core.Settings
  Core.Core
  Core.Adjunction.

Record Monad (C : Category) : Type :=
  { asFunctor :> Functor C C
  ; F := asFunctor
  ; ret : forall {a : C}, hom C a (F a)
  ; join : forall {a : C}, hom C (F (F a)) (F a)
  ; ret_join : forall {a : C},
      ret >>> join (a := a) =~ id C
  ; map_ret_join : forall {a : C},
      map F ret >>> join (a := a) =~ id C
  ; join_join : forall {a : C},
      map F (join (a := a)) >>> join =~ join >>> join
  ; natural_ret : forall {a b : C} (f : hom C a b),
      f >>> ret =~ ret >>> map F f
  ; natural_join : forall {a b : C} (f : hom C a b),
      map F (map F f) >>> join =~ join >>> map F f
  }.
Arguments Build_Monad C asFunctor &.
