(** * Monoid objects *)

From Kittens Require Import
  Core.Settings
  Core.Core
  Core.Tensor
  Category.Product.

Record MonoidObject (C : Category) (T : Tensor C) : Type :=
  { M :> C
  ; mul : hom C (T (M, M)) M
  ; one : hom C (Tensor.I T) M
  ; mul_one_l : counit_l T >>> bimap T one (id C) >>> mul =~ id C
  ; mul_one_r : counit_r T >>> bimap T (id C) one >>> mul =~ id C
  ; mul_assoc : bimap T mul (id C) >>> mul =~ assoc T >>> bimap T (id C) mul >>> mul
  }.
Arguments Build_MonoidObject C T &.
