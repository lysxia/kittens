# Kittens

Category theory library in Coq with bundled structures: good for formalizing
results that are small and cute, like kittens.

## Build

```
dune build
```

## Structure

Under `theories/`:

- `Core/`: core definitions
- `Category/`: examples of categories
- `Kitty/`: facts about the category of types (from `Category.Type`)

## Fun facts

### A monad is just a monoid in the category of endofunctors, what's the problem?

That isomorphism between monads and monoid objects is defined in
[`Core/MonadFacts.v`](./theories/Core/MonadFacts.v).

Variations (TODO):

- A monoidal category is just a monoid in the category of endofunctors.
- A category is just a monoid in the category of graphs
- A monoid is just a monoid in the category of types

### Lists for free

Deriving operations for lists by abstract nonsense.

- Lists are the free monoid, an adjunction, therefore lists are a monad.
  ([`Kitty/FreeMonoid.v`](./theories/Kitty/FreeMonoid.v))

- Lists are isomorphic to a free writer monad, for an alternative
  interpretation of the "free monad" as a monad: `(>>=)` for lists is given by
  a fold of the free monad, whereas `(>>=)` for the writer
  monad corresponds to `(++)` for lists. ([`Kitty/FreeMonad.v`](./theories/Kitty/FreeMonad.v))

## Related links

Formalizations of category theory:

- [*Formalizing Category Theory in Agda*](https://hustmphrrr.github.io/asset/papers/cpp21.pdf),
  by Jason Hu and Jacques Carette (CPP 2021).
  ([*agda-categories*](https://github.com/agda/agda-categories/))

- [*Experience Implementing a Performant Category-Theory Library in Coq*](https://arxiv.org/abs/1401.7694),
  by Jason Gross, Adam Chlipala, and David Spivak (ITP 2014).
  ([*coq-hott*](https://github.com/HoTT/HoTT/))

- [*Type Classes for Mathematics in Type Theory*](https://arxiv.org/abs/1102.1323),
  by Bas Spitters and Eelis van der Weegen (MSCS 2011).
  ([*coq-math-classes*](https://github.com/coq-community/math-classes))

- [*Constructive category theory*](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.39.4193),
  by Gérard Huet and Amokrane Saïbi (1998).
  ([*coq-concat*](https://github.com/coq-contrib/concat))
